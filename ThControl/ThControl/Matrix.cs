﻿using System;
using System.Windows.Forms;

namespace ThControl
{
    public class Matrix
    {
        public readonly int CountRows;    //количество строк
        public readonly int CountColumns; //количество столбцов
        public double[,] _matrix;
        public string NameMatrix;

        public Matrix(DataGridView data)
        {
            NameMatrix = "";
            CountRows = data.RowCount;
            CountColumns = data.ColumnCount;
            _matrix = new double [CountRows, CountColumns];
            SetMatrix(data);
        }

        public Matrix(int countRows, int countColumns)
        {
            CountRows = countRows;
            CountColumns = countColumns;
            NameMatrix = "";
            _matrix = new double[countRows, countColumns];
        }

        private void SetMatrix(DataGridView data)
        {
            for (var i = 0; i < CountRows; i++)
            {
                for (var j = 0; j < CountColumns; j++)
                {
                    _matrix[i, j] = Convert.ToDouble(data[j, i].Value);
                }
            }
        }

        public static  Matrix operator + (Matrix a, Matrix b)
        {
            if (a.CountRows != b.CountRows || a.CountColumns != b.CountColumns)
            {
                throw new Exception("Сложение невозможно");
            }

            var result = new Matrix(a.CountRows, a.CountColumns);
         
            for(var i=0; i<a.CountRows; i++)
            {
                for (var j=0; j<a.CountColumns; j++)
                {
                    result._matrix[i,j] = a._matrix[i,j] + b._matrix[i, j];
                }
            }

            return result;
        }

        public static Matrix operator *(Matrix a, Matrix b)
        {
            var result = new Matrix(a.CountRows, b.CountColumns);
            
            for (var i = 0; i < a.CountRows; i++)
            {
                for (var j = 0; j < b.CountColumns; j++)
                {
                    double sum = 0;
                    for(var k=0;k<a.CountColumns; k++)
                    {
                        sum = sum + a._matrix[i, k] * b._matrix[k, j];
                    }
                    result._matrix[i, j] = sum;
                }
            }

            return result;
        }

        public double Determinant()
        {
            if (CountColumns != CountRows)
            {
                throw new Exception("Расчет определителя невозможен");
            }
            return Determinant(_matrix);
        }

        private double Determinant(double[,] array)
        {
            var n = (int)Math.Sqrt(array.Length);

            if (n == 1)
            {
                return array[0, 0];
            }

            double det = 0;

            for (var k = 0; k < n; k++)
            {
                det += array[0, k] * Cofactor(array, 0, k);
            }

            return det;
        }

        private double Cofactor(double[,] array, int row, int column)
        {
            return Convert.ToDouble(Math.Pow(-1, column + row)) * Determinant(Minor(array, row, column));
        }

        private static double[,] Minor(double[,] array, int row, int column)
        {
            var n = (int)Math.Sqrt(array.Length);
            var minor = new double[n - 1, n - 1];

            var index0 = 0;
            for (var i = 0; i < n; i++)
            {
                if (i == row)
                {
                    continue;
                }
                var index1 = 0;
                for (var j = 0; j < n; j++)
                {
                    if (j == column)
                    {
                        continue;
                    }
                    minor[index0, index1] = array[i, j];
                    index1++;
                }
                index0++;
            }
            return minor;
        }

        public Matrix Transpose()
        {
            var transposedMatrix = new Matrix(CountRows,CountColumns);

            for (var i = 0; i < CountRows; i++)
            {
                for (var j = 0; j < CountColumns; j++)
                {
                   transposedMatrix._matrix[j, i] = _matrix[i, j];
                }
            }
            return transposedMatrix;
        }

        public Matrix Inverse()
        {
            var det = Determinant();
            if (det == 0)
            {
                throw new Exception("Матрица вырождена");
            }

            var inversedMatrix = new Matrix(CountRows, CountColumns);

            for (var i = 0; i < CountRows; i++)
            {
                for (var j = 0; j < CountColumns; j++)
                {
                    inversedMatrix._matrix[i, j] = Cofactor(_matrix, i, j) / det;
                }
            }

            return inversedMatrix.Transpose();
        }

        public void RandomSetMatrixValue()
        {
            var randomSeed = new Random();
            for (var i = 0; i < CountRows; i++)
            {
                for (var j = 0; j < CountColumns; j++)
                {
                    _matrix[i, j] = randomSeed.Next(10);
                }
            }
        }
    }
}
