﻿namespace ThControl
{
    public enum Operations
    {
        Multiplication,
        Addition,
        Inverse,
        Determinant,
        Transpose,
        Eigenvalues,
        Eigenvectors
    } 
}
