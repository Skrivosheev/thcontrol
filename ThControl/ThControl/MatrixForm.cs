﻿using System;
using System.Windows.Forms;

namespace ThControl
{
    public partial class MatrixForm : Form
    {
        private readonly MainForm _mainForm;
        private string _nameMatrix;
        public MatrixForm()
        {
            InitializeComponent();
        }

        public MatrixForm(MainForm f)
        {
            InitializeComponent();
            _mainForm = f;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.RowCount = Convert.ToInt32(textBox2.Text);
            dataGridView1.ColumnCount = Convert.ToInt32(textBox1.Text);
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.Visible = true;
            button2.Visible = true;
            _nameMatrix = textBox3.Text;
            label4.Text = @"Matrix: " + _nameMatrix;
            label4.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _nameMatrix = textBox3.Text;
            var additionalMatrix = new Matrix(dataGridView1) {NameMatrix = _nameMatrix};
            _mainForm.ListOfMatrixes.Add(additionalMatrix);

            _mainForm.comboBox1.Items.Add(_nameMatrix);
            _mainForm.comboBox2.Items.Add(_nameMatrix);

            _mainForm.CountMatrix++;
            Hide();
        }
    }
}
