﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;

namespace ThControl
{
    public partial class MainForm : Form
    {
        public List<Matrix> ListOfMatrixes=new List<Matrix>();

        public Matrix FirstOperand;
        public Matrix SecondOperand;
        public Matrix Result;
        public int CountMatrix;

        public MainForm()
        {
            InitializeComponent();
        }

        private void задатьМатрицуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var matrixForm = new MatrixForm(this);
            matrixForm.ShowDialog();
            Refresh();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedState = comboBox1.SelectedItem.ToString();
            var matrix = ListOfMatrixes.Find(selectedMatrix => selectedMatrix.NameMatrix == selectedState);
            SetDataGridView(matrix, dataGridView1);
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            FirstOperand = matrix;
        }

        private static void SetDataGridView(Matrix matrix, DataGridView dataGridView)
        {
            dataGridView.ColumnCount = matrix.CountColumns;
            dataGridView.RowCount = matrix.CountRows;
            for (var i = 0; i < dataGridView.RowCount; i++)
            {
                for (var j = 0; j < dataGridView.ColumnCount; j++)
                {
                    dataGridView[j, i].Value= matrix._matrix[i, j];
                }
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedState = comboBox2.SelectedItem.ToString();
            var matrix = ListOfMatrixes.Find(selectedMatrix => selectedMatrix.NameMatrix == selectedState);
            SetDataGridView(matrix, dataGridView2);
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            SecondOperand = matrix;
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedStateInBox3 = comboBox3.SelectedItem.ToString();
            string historyMessage;
            if (selectedStateInBox3==Operations.Determinant.ToString())
            {
                var selectedState = comboBox4.SelectedItem.ToString();
                var matrix = ListOfMatrixes.Find(selectedMatrix => selectedMatrix.NameMatrix == selectedState);
                SetDataGridView(matrix, dataGridView3);
                dataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                label3.Text = @"Determinant: " + matrix.NameMatrix + @" = " +
                              matrix.Determinant().ToString(CultureInfo.InvariantCulture);
            } 
            else if (selectedStateInBox3 == Operations.Transpose.ToString())
            {
                var selectedState = comboBox4.SelectedItem.ToString();
                var matrix = ListOfMatrixes.Find(selectedMatrix => selectedMatrix.NameMatrix == selectedState);
                var result = matrix.Transpose();
                SetDataGridView(result, dataGridView3);
                dataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                result.NameMatrix += matrix.NameMatrix + @"^T";

                historyMessage = result.NameMatrix;
                label3.Text = @"Result: " + historyMessage;

                ListOfMatrixes.Add(result);
                comboBox1.Items.Add(historyMessage);
                comboBox2.Items.Add(historyMessage);

                var additionalItem = new ToolStripMenuItem(historyMessage);
                additionalItem.Click += additionalItem_Click;
                historyToolStripMenuItem.DropDownItems.Add(additionalItem);
            }
            else if (selectedStateInBox3 == Operations.Inverse.ToString())
            {
                var selectedState = comboBox4.SelectedItem.ToString();
                var matrix = ListOfMatrixes.Find(selectedMatrix => selectedMatrix.NameMatrix == selectedState);
                var result = matrix.Inverse();
                SetDataGridView(result, dataGridView3);
                dataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                result.NameMatrix += matrix.NameMatrix + @"^-1";

                historyMessage = result.NameMatrix;
                label3.Text = @"Result: " + historyMessage;

                ListOfMatrixes.Add(result);
                comboBox1.Items.Add(historyMessage);
                comboBox2.Items.Add(historyMessage);

                var additionalItem = new ToolStripMenuItem(historyMessage);
                additionalItem.Click += additionalItem_Click;
                historyToolStripMenuItem.DropDownItems.Add(additionalItem);
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedState = comboBox3.SelectedItem.ToString();           
            var historyMessage="";
            comboBox4.Visible = false;

            if (selectedState == Operations.Addition.ToString())
            {
                Result = FirstOperand + SecondOperand;
                Result.NameMatrix = FirstOperand.NameMatrix + " " + "+" + " " +
                                    SecondOperand.NameMatrix;
                historyMessage = Result.NameMatrix;
            }
            else if (selectedState == Operations.Multiplication.ToString())
            {
                Result = FirstOperand * SecondOperand;
                Result.NameMatrix = FirstOperand.NameMatrix + " " + "*" + " " +
                                    SecondOperand.NameMatrix;
                historyMessage = Result.NameMatrix;
            }
            else if (selectedState == Operations.Determinant.ToString())
            {
                comboBox4.Items.Clear();
                foreach (var val in comboBox1.Items)
                {
                    comboBox4.Items.Add(val);
                }

                comboBox4.Visible = true;
            }
            else if (selectedState == Operations.Inverse.ToString())
            {
                comboBox4.Items.Clear();
                foreach (var val in comboBox1.Items)
                {
                    comboBox4.Items.Add(val);
                }

                comboBox4.Visible = true;
            }
            else if (selectedState == Operations.Transpose.ToString())
            {
                comboBox4.Items.Clear();
                foreach (var val in comboBox1.Items)
                {
                    comboBox4.Items.Add(val);
                }

                comboBox4.Visible = true;
            }

            if (selectedState == Operations.Determinant.ToString() || selectedState == Operations.Transpose.ToString() || selectedState == Operations.Inverse.ToString()) return;

            ListOfMatrixes.Add(Result);

            SetDataGridView(Result, dataGridView3);
            dataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;



            comboBox1.Items.Add(historyMessage);
            comboBox2.Items.Add(historyMessage);

            var additionalItem = new ToolStripMenuItem(historyMessage);
            additionalItem.Click += additionalItem_Click;
            historyToolStripMenuItem.DropDownItems.Add(additionalItem);

            label3.Text = @"Result: " + historyMessage;
        }

        private void additionalItem_Click(object sender, EventArgs e)
        {
            var menuItem = sender as ToolStripMenuItem;
            var matrix = ListOfMatrixes.Find(selectedMatrix => menuItem != null && selectedMatrix.NameMatrix == menuItem.Text);
            SetDataGridView(matrix,dataGridView3);
            label3.Text = @"Result: " + matrix.NameMatrix;
        }
    }
}
